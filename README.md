# BetterHealth

## Description

BetterHealth ist eine Applikation, wodurch Ernährungspläne basierend auf User-Informationen mit KI generiert werden können.
Es ist eine Web-Applikation, die mit Django und ReactJS umgesetzt wurde.


## Setup

### Frontend

Install latest LTS nodejs version.

Then install the dependencies:
```
npm install --legacy-peer-deps
```

To start the frontend, run:
```
npm start
```

### Backend
Install Python version 3.11.5

Install dependencies and setup envs:
```
pip install pipenv
pipenv install
cp .env.example .env
```
Edit the values in the .env file accordingly. You may need to generate an API token for OpenAI.

Enter the virtual environment:
```
pipenv shell
```

Apply the database migrations:
```
cd api
python manage.py migrate
```

To start the backend, run:

```
python manage.py runserver
```
