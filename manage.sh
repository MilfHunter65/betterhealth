#!/bin/bash

# Script to start or stop a Docker Compose BetterHealth
# Usage: ./docker-compose-control.sh start|stop|status

# Define the Docker Compose file path
COMPOSE_FILE="docker-compose.yml"

# Check if the file exists
if [ ! -f "$COMPOSE_FILE" ]; then
  echo "Error: $COMPOSE_FILE not found in the current directory."
  exit 1
fi

# Define the function to start Docker Compose
start_compose() {
  echo "Starting Docker Compose..."
  docker-compose up -d
  if [ $? -eq 0 ]; then
    echo "Docker Compose started successfully."
  else
    echo "Failed to start Docker Compose."
    exit 1
  fi
}

# Define the function to stop Docker Compose
stop_compose() {
  echo "Stopping Docker Compose..."
  docker-compose down
  if [ $? -eq 0 ]; then
    echo "Docker Compose stopped successfully."
  else
    echo "Failed to stop Docker Compose."
    exit 1
  fi
}

# Define the function to check the status
status_compose() {
  echo "Checking Docker Compose status..."
  docker-compose ps
}

# Check the command-line argument
case "$1" in
  start)
    start_compose
    ;;
  stop)
    stop_compose
    ;;
  status)
    status_compose
    ;;
  *)
    echo "Usage: $0 start|stop|status"
    exit 1
    ;;
esac

exit 0
