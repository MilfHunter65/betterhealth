FROM python:3.11.5-slim

RUN apt-get update
RUN apt-get install -y pkg-config default-libmysqlclient-dev gcc
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
RUN mkdir /app
WORKDIR /app
RUN pip install --upgrade pip
COPY ./api/ .
COPY ./Pipfile .
COPY ./Pipfile.lock .

RUN pip install pipenv
RUN pipenv install --deploy --system
RUN python manage.py collectstatic --noinput

EXPOSE 8000
