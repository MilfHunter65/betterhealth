// Importing necessary dependencies and components
import "./register.css"; // Custom CSS for the registration page
import Header from "../../components/header/header"; // Header component
import Footer from "../../components/footer/footer"; // Footer component
import Spinner from "react-bootstrap/Spinner"; // Bootstrap spinner component for loading indication
import { Modal } from "react-bootstrap"; // Bootstrap modal component for popups
import React, { useState } from "react"; // React and useState hook for state management
import { registerUser, obtainToken } from "../../services/apiService"; // API service functions for user registration and token obtaining
import "bootstrap/dist/css/bootstrap.min.css"; // Bootstrap CSS for styling
import history from "../../services/history"; // History service for navigation
import BackButton from "../../components/backButton/backButton"; // Button to return to previous site

// Main Register component function
export default function Register() {
    // State variables for managing form inputs and UI state
    const [male, setMale] = useState(true);
    const [email, setEmail] = useState("");
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [leftPassword, setLeftPassword] = useState("");
    const [rightPassword, setRightPassword] = useState("");
    const [passwordIsIdentical, setPasswordIsIdentical] = useState(true);
    const [size, setSize] = useState(0);
    const [age, setAge] = useState(0);
    const [weight, setWeight] = useState(0);
    const [goalWeight, setGoalWeight] = useState(0);
    const [activities, setActivities] = useState(0);
    const [endDate, setEndDate] = useState(null);
    const [isLoading, setIsLoading] = useState(false);
    const [showPopup, setShowPopup] = useState(false);
    const [popupText, setPopupText] = useState("");

    // Regular expression for validating password
    const validPassword = new RegExp(
        "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$"
    );

    const [isValid, setIsValid] = useState(true);

    /**
     * Function to format date to 'YYYY-MM-DD' string
     * @param {Date} date - The date object to format
     * @returns {string} - The formatted date string
     */
    const formatDate = (date) => {
        if (!(date instanceof Date)) {
            throw new Error("Invalid date");
        }

        const year = date.getFullYear();
        const month = String(date.getMonth() + 1).padStart(2, "0"); // getMonth() is zero-based
        const day = String(date.getDate()).padStart(2, "0");

        return `${year}-${month}-${day}`;
    };

    /**
     * Calculates the daily calorie requirement based on the Basal Metabolic Rate (BMR)
     * and Activity Metabolic Rate (AMR) using the Harris-Benedict equation.
     *
     * @returns {number} AMR - The calculated daily calorie requirement.
     */
    const calculateCalReq = () => {
        // Initialize BMR (Basal Metabolic Rate)
        let BMR = 0;

        // Calculate BMR based on gender
        if (male) {
            BMR = 655.1 + 9.563 * weight + 1.85 * size - 4.676 * age;
        } else {
            BMR = 66.47 + 13.75 * weight + 5.003 * size - 6.755 * age;
        }

        // Initialize AMR (Activity Metabolic Rate)
        let AMR = 0;

        // Calculate AMR based on activity level
        if (activities == 0) {
            AMR = BMR * 1.2; // Sedentary (little or no exercise)
        } else if (activities == 1) {
            AMR = BMR * 1.375; // Lightly active (light exercise/sports 1-3 days/week)
        } else if (activities <= 3) {
            AMR = BMR * 1.55; // Moderately active (moderate exercise/sports 3-5 days/week)
        } else if (activities <= 7) {
            AMR = BMR * 1.725; // Very active (hard exercise/sports 6-7 days a week)
        } else {
            AMR = BMR * 1.9; // Super active (very hard exercise/sports & physical job or 2x training)
        }

        return Math.round(AMR);
    };

    // useEffect hook to check if passwords match and meet criteria
    React.useEffect(() => {
        if (
            leftPassword === rightPassword &&
            validPassword.test(leftPassword)
        ) {
            setPasswordIsIdentical(true);
        } else {
            setPasswordIsIdentical(false);
        }
    }, [leftPassword, rightPassword]);

    /**
     * Function to handle user registration
     * @returns {Promise<void>} - Returns a promise that resolves when the user registration is complete
     */
    const register = async () => {
        try {
            const currentDate = new Date();
            const chosenEndDate = new Date(endDate);
            // start the loading process and register the user
            setIsLoading(true);
            // Send necessary data to backend to create user
            await registerUser({
                username: email,
                email: email,
                password: leftPassword,
                first_name: firstName,
                last_name: lastName,
                male: male,
                weight: weight,
                size: size,
                calorie_requirement: calculateCalReq(),
                activity_hours_per_week: activities,
                start_date: formatDate(currentDate),
                end_date: formatDate(chosenEndDate),
                goal_weight: goalWeight,
            });

            // Wait to obtain the token generated for the new user
            await obtainToken({
                username: email,
                password: leftPassword,
            });
            // After the token has been returned -> end loading process and redirect to overview
            setIsLoading(false);
            history.push("/");
        } catch (error) {
            // Handle errors
            setIsLoading(false);
            setShowPopup(true);
            if (error.message === "User already exists") {
                setPopupText(
                    "Es existiert bereits ein Benutzer mit dieser Email. Bitte versuchen Sie, sich anzumelden oder verwenden Sie eine andere Email."
                );
            } else {
                setPopupText(String(error));
            }
        }
    };

    /**
     * Function to validate form inputs
     * @returns {boolean} - Returns true if inputs are valid, false otherwise
     */
    const validateInput = () => {
        if (
            email === "" ||
            firstName === "" ||
            lastName === "" ||
            size <= 0 ||
            weight <= 0 ||
            goalWeight <= 0 ||
            activities < 0 ||
            endDate == null ||
            endDate < 1 ||
            age < 1
        ) {
            setIsValid(false);
            return false;
        }
        setIsValid(true);
        return true;
    };

    // JSX return statement to render the component
    return (
        <div>
            <header>
                <Header title="Registrierung" />
            </header>
            <div className="backButtonDiv">
                <BackButton />
            </div>
            <div>
                {/*If isLoading, show loading spinner*/}
                {isLoading ? (
                    <>
                        <div className="spinnerContainer">
                            <Spinner
                                animation="border"
                                style={{
                                    width: "100px",
                                    height: "100px",
                                    borderWidth: "10px",
                                }}
                                className="spinner"
                            />
                        </div>
                        <h4 className="spinnerText">Bitte warten...</h4>
                    </>
                ) : (
                    <div>
                        <div className="verticalContainer">
                            <h3>Email</h3>
                            <input
                                className="inputFieldBig"
                                type="email"
                                id="emailInput"
                                onChange={(event) =>
                                    // Update variable on input change
                                    setEmail(event.target.value)
                                }
                            />

                            <h3>Vorname</h3>
                            <input
                                className="inputFieldBig"
                                type="text"
                                onChange={(event) =>
                                    // Update variable on input change
                                    setFirstName(event.target.value)
                                }
                            />
                            <h3>Nachname</h3>
                            <input
                                className="inputFieldBig"
                                type="text"
                                onChange={(event) =>
                                    // Update variable on input change
                                    setLastName(event.target.value)
                                }
                            />
                            <h3>Password</h3>
                            <p className="passwordInfo">
                                Passwort muss mindestens 8 Zeichen lang sein,
                                sowie mindestens einen Buchstaben, eine Zahl und
                                ein Sonderzeichen enthalten.
                            </p>
                            <input
                                className={
                                    passwordIsIdentical
                                        ? "inputFieldBig"
                                        : "invalidInputFieldBig"
                                }
                                type="password"
                                id="passwordInput1"
                                onChange={(event) =>
                                    // Update variable on input change
                                    setLeftPassword(event.target.value)
                                }
                            />
                            <h3>Passwort wiederholen</h3>
                            <input
                                className={
                                    passwordIsIdentical
                                        ? "inputFieldBig"
                                        : "invalidInputFieldBig"
                                }
                                type="password"
                                id="passwordInput2"
                                onChange={(event) =>
                                    // Update variable on input change
                                    setRightPassword(event.target.value)
                                }
                            />
                            <h3>Geschlecht</h3>
                            <div className="gendersHolder">
                                <div className="genderHolder">
                                    Männlich
                                    <input
                                        className="inputFieldSmall"
                                        value="männlich"
                                        type="radio"
                                        checked={male}
                                        onChange={(event) =>
                                            // Update variable on input change
                                            setMale(event.target.value)
                                        }
                                    />
                                </div>
                                <div className="genderHolder">
                                    Weiblich
                                    <input
                                        className="inputFieldSmall"
                                        value="weiblich"
                                        type="radio"
                                        checked={!male}
                                        onChange={(event) =>
                                            // Update variable on input change
                                            setMale(!event.target.value)
                                        }
                                    />
                                </div>
                            </div>
                            <h3>Grösse</h3>
                            <input
                                className="inputFieldSmall"
                                type="number"
                                onChange={(event) =>
                                    // Update variable on input change
                                    setSize(event.target.value)
                                }
                            />
                            <h3>Alter</h3>
                            <input
                                className="inputFieldSmall"
                                type="number"
                                onChange={(event) =>
                                    // Update variable on input change
                                    setAge(event.target.value)
                                }
                            />
                            <h3>Gewicht</h3>
                            <input
                                className="inputFieldSmall"
                                type="number"
                                onChange={(event) =>
                                    // Update variable on input change
                                    setWeight(event.target.value)
                                }
                            />
                            <h3>Zielgewicht</h3>
                            <input
                                className="inputFieldSmall"
                                type="number"
                                onChange={(event) =>
                                    // Update variable on input change
                                    setGoalWeight(event.target.value)
                                }
                            />
                            <h3>Anzahl Stunden aktivität pro Woche</h3>
                            <input
                                className="inputFieldSmall"
                                onChange={(event) =>
                                    // Update variable on input change
                                    setActivities(event.target.value)
                                }
                                type="number"
                            />
                            <h3>Zieldatum</h3>
                            <input
                                type="date"
                                className="inputFieldBig"
                                onChange={(event) =>
                                    // Update variable on input change
                                    setEndDate(event.target.value)
                                }
                            />
                        </div>

                        <h3 className="invalidInputText">
                            {!isValid
                                ? "Invalide Angaben, bitte überprüfe deine Eingaben"
                                : ""}
                        </h3>

                        <div>
                            <button
                                className="Button"
                                onClick={() => {
                                    // If all inputs are valid, start registration
                                    if (validateInput()) {
                                        register();
                                    }
                                }}>
                                Registrieren
                            </button>
                        </div>
                    </div>
                )}
            </div>
            <Modal show={showPopup}>
                <Modal.Header>
                    <Modal.Title style={{ color: "red" }}>Error</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <p>{popupText}</p>
                </Modal.Body>

                <Modal.Footer>
                    <button
                        style={{
                            background: "white",
                            color: "red",
                            borderRadius: "15px",
                            borderColor: "red",
                            paddingTop: "10px",
                            paddingBottom: "10px",
                            paddingLeft: "20px",
                            paddingRight: "20px",
                        }}
                        onClick={() => setShowPopup(false)}>
                        Close
                    </button>
                </Modal.Footer>
            </Modal>

            <div className="footerDiv">
                <Footer />
            </div>
        </div>
    );
}
