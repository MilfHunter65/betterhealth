// Import necessary CSS module for styling
import styles from "./overview.module.css";
// Import Calendar component
import Calendar from "../../components/calendar/calendar";
// Import Header component
import Header from "../../components/header/header";
// Import Footer component
import Footer from "../../components/footer/footer";
// Import ProfileButton component
import ProfileButton from "../../components/profileButton/profileButton";

/**
 * Overview component renders the main overview page including a header, profile button, calendar, and footer.
 * @returns {JSX.Element} The Overview component.
 */
export default function Overview() {
    return (
        <div>
            {/* Render Header component with title "Übersicht" */}
            <Header title="Übersicht" />

            {/* Render ProfileButton component with custom styling */}
            <div className={styles.profileBtnOverview}>
                <ProfileButton />
            </div>

            {/* Main container for the overview section */}
            <div className={styles.overviewContainer}>
                {/* Title for the overview section */}
                <div className={styles.overviewTitle}>
                    <h3>Ernährungspläne</h3>
                </div>
                {/* Render Calendar component */}
                <Calendar />
            </div>

            {/* Render Footer component */}
            <Footer />
        </div>
    );
}
