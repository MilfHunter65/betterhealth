// Import necessary modules and components
import styles from "./login.module.css"; // Import CSS module for styling
import Header from "../../components/header/header"; // Import Header component
import { Link } from "react-router-dom"; // Import Link component for navigation

import Footer from "../../components/footer/footer"; // Import Footer component
import Alert from 'react-bootstrap/Alert'; // Import Alert from bootstrap for error messages
import { useState } from "react"; // Import useState hook for managing state
import { obtainToken } from "../../services/apiService"; // Import API service to obtain token
import history from "../../services/history"; // Import history service for navigation

export default function Login() {
    // Define state variables for email, password, and errors
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [errors, setErrors] = useState("");

    /**
     * Handles the form submission for login.
     * @param {Object} event - The event object from the form submission.
     * @returns {void}
     */
    const handleSubmit = async (event) => {
        event.preventDefault(); // Prevent default form submission
        try {
            // Attempt to obtain a token with the provided email and password
            await obtainToken({ username: email, password: password });
            history.push("/"); // Redirect to the home page on success
        } catch (e) {
            console.error(e);
            // Set error messages based on the error code
            if (e.code === "invalid") {
                setErrors("Email und Passwort eingeben");
            } else if (e.code === "") {
                setErrors("Email oder Password falsch");
            } else {
                setErrors("Ein Fehler ist aufgetreten");
            }
        }
    };

    return (
        <div>
            {/* Render Header component with title "Login" */}
            <Header title="Login" />

            {/* Display errors if any */}
            {errors && <div className={styles.errorContainer}><Alert variant="danger">{errors}</Alert></div>}

            {/* Form for user login */}
            <form onSubmit={handleSubmit}>
                <div>
                    <h2 className={styles.emailTitle}>Email</h2>
                    {/* Input field for email */}
                    <input
                        className={styles.inputFields}
                        type="email"
                        id="emailInput"
                        // updating the variable with the value, if its changed
                        onChange={(e) => setEmail(e.target.value)}
                    />
                </div>

                <div>
                    <h2>Password</h2>
                    {/* Input field for password */}
                    <input
                        className={
                            styles.inputFields + " " + styles.passwordInput
                        }
                        type="password"
                        // updating the variable with the value, if its changed
                        onChange={(e) => setPassword(e.target.value)}
                    />
                </div>

                <div>
                    {/* Submit button for the form */}
                    <button type="submit" className={styles.buttons}>
                        Login
                    </button>
                </div>
            </form>

            <div>
                {/* Link to the registration page */}
                <Link to="/register" className={styles.buttons}>
                    Registrieren
                </Link>
            </div>

            {/* Render Footer component with title "Login" */}
            <Footer title="Login" />
        </div>
    );
}
