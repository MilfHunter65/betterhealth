// Import the Outlet component from react-router-dom
import {Outlet} from "react-router-dom";

// Define the Layout component
function Layout() {
    // Render the Outlet component, which will render the matched child route element
    return (
        <Outlet />
    );
}
// Export the Layout component as the default export
export default Layout;
