// Import necessary CSS file for the component
import './recipe.css';
// Import Header, Footer and BackButton components
import Header from '../../components/header/header';
import Footer from '../../components/footer/footer';
import BackButton from '../../components/backButton/backButton';
// Import hooks and functions from React and React-Router-Dom
import { useParams } from 'react-router-dom';
import { useEffect, useState } from 'react';
// Import API service function to fetch recipe data
import { getRecipe } from '../../services/apiService';


// Define the Recipe component as the default export
export default function Recipe() {
    // Use useParams hook to extract the recipe ID from the URL
    const recipeId = useParams();
    // Define state variables for the recipe, number of persons, and adjusted ingredients
    const [recipe, setRecipe] = useState(undefined);
    const [numberOfPersons, setNumberOfPersons] = useState('1');
    const [adjustedIngredients, setAdjustedIngredients] = useState([]);

    // useEffect hook that runs on the first render and when the recipe ID changes
    useEffect(() => {
        // Asynchronous function to fetch the recipe data from the API + returns a Promise object, which we don't need
        async function fetchRecipe() {
            const recipe_json = await getRecipe(recipeId.id);
            setRecipe(recipe_json);
            setAdjustedIngredients(recipe_json.recipe_ingredients);
        }
        fetchRecipe();
    }, [recipeId]);

    // Event handler for changing the number of persons input
    const handleInputChange = (event) => {
        const value = event.target.value;

        // Prevent input longer than two characters, prevent numbers smaller than 1
        if (value.length > 2 || parseInt(value) < 1) {
            return;
        }

        setNumberOfPersons(value);
    };

    // useEffect hook that runs when the number of persons changes
    useEffect(() => {
        generateNewRecipeDependingOnNumberOfPersons();
    }, [numberOfPersons]);

    // Function to adjust the ingredient amounts based on the number of persons
    const generateNewRecipeDependingOnNumberOfPersons = () => {
        const number = parseInt(numberOfPersons);

        if (recipe) {
            // Ensure the number of persons is at least 1
            const personCount = isNaN(number) || number <= 0 ? 1 : number;
            // Calculate the new ingredient amounts
            const newIngredients = recipe.recipe_ingredients.map(ingredient => ({
                ...ingredient,
                amount: (ingredient.amount * personCount)
            }));
            setAdjustedIngredients(newIngredients);
        }
    };

    // Render the component
    return (
        <div>
            <Header title="Rezept" />
            <div className='backButtonDiv'>
            <BackButton />
            </div>
            <h2 id="recipe-title">{/* Conditionally render the recipe name if available */ recipe && recipe.name}</h2>
            <div id="recipe-number-of-persons">
                <p >Rezept für <input type="number" value={numberOfPersons} onChange={handleInputChange} id="recipe-number-of-persons-value" />{
                    // Singular if only 1 Person was selected, otherwise in plural
                    numberOfPersons < 2 ? "Person" : "Personen"
                }</p>
            </div>
            <div id="recipe-ingredients">
                <h2 className='recipe-title'>Zutaten</h2>
                <ul id="recipe-ingredients-list">
                    {/* Conditionally render the list of ingredients if the recipe is available */ recipe && adjustedIngredients.map(recipeIngredient =>
                        <li key={recipeIngredient.ingredient.id}>{recipeIngredient.ingredient.name} {recipeIngredient.amount} {recipeIngredient.amount_unit}</li>
                    )}
                </ul>
            </div>
            <div id="recipe-preparation">
                <h2 className='recipe-title'>Zubereitung</h2>
                <p id='recipe-preparation-text'>{/* Conditionally render the preparation instructions if available */ recipe && recipe.preparation}</p>
            </div>
            <div id='recipe-nutrition-values'>
                <h2 className='recipe-title'>Nährwerte (für 1 Person)</h2>
                <div>
                    <p className='recipe-nutrition-values-text'>
                        Energie: {/* Conditionally render the energy value if available */ recipe && recipe.nutrition_value.energy.toFixed(2)} kj
                    </p>
                    <p className='recipe-nutrition-values-text'>
                        Fett: {/* Conditionally render the fat value if available */ recipe && recipe.nutrition_value.fat.toFixed(2)} g
                    </p>
                    <p className='recipe-nutrition-values-text'>
                        Kohlenhydrate: {/* Conditionally render the carbohydrates value if available */ recipe && recipe.nutrition_value.carbohydrates.toFixed(2)} g
                    </p>
                    <p className='recipe-nutrition-values-text'>
                        Ballaststoffe: {/* Conditionally render the dietary_fibers value if available */ recipe && recipe.nutrition_value.dietary_fibers.toFixed(2)} g
                    </p>
                    <p className='recipe-nutrition-values-text'>
                        Proteine: {/* Conditionally render the protein value if available */ recipe && recipe.nutrition_value.protein.toFixed(2)} g
                    </p>
                    <p className='recipe-nutrition-values-text'>
                        Salz: {/* Conditionally render the salt value if available */ recipe && recipe.nutrition_value.salt.toFixed(2)} g
                    </p>
                    <p className='recipe-nutrition-values-text'>
                        Kalorien: {/* Conditionally render the calories value if available */ recipe && recipe.nutrition_value.calories.toFixed(2)} kcal
                    </p>
                </div>
            </div>
            <Footer />
        </div>
    );
}
