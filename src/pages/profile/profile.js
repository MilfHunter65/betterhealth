import styles from './profile.module.css'; // Importing CSS module for styling
import Header from '../../components/header/header'; // Importing Header component
import Footer from '../../components/footer/footer'; // Importing Footer component
import BackButton from '../../components/backButton/backButton'; // Importing BackButton component
import EditButton from "../../components/editButton/editButton"; // Importing EditButton component
import {getProfile, logout, updateProfile} from "../../services/apiService"; // Importing API service functions
import {useEffect, useState} from "react"; // Importing React hooks

/**
 * Profile component renders the profile page.
 * @returns {JSX.Element} The Profile component.
 */
export default function Profile() {
    // Defining state variables
    const [male, setMale] = useState(true);
    const [email, setEmail] = useState("");
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [size, setSize] = useState(0);
    const [weight, setWeight] = useState(0);
    const [goalWeight, setGoalWeight] = useState(0);
    const [activities, setActivities] = useState(0);
    const [calories, setCalories] = useState(0);
    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(new Date());
    const [isValid, setIsValid] = useState(true);
    const [editMode, setEditMode] = useState(false);


    /**
     * Sets profile values based on the provided JSON object.
     *
     * @param {Object} profileJson - The JSON object containing profile information.
     */
    function setProfileValues(profileJson) {
        setEmail(profileJson.email);
        setFirstName(profileJson.first_name);
        setLastName(profileJson.last_name);
        setMale(profileJson.userinfos.male);
        setWeight(profileJson.userinfos.weight);
        setSize(profileJson.userinfos.size);
        setCalories(profileJson.userinfos.calorie_requirement);
        setActivities(profileJson.userinfos.activity_hours_per_week);
        setStartDate(new Date(profileJson.userinfos.goal.start_date));
        setEndDate(new Date(profileJson.userinfos.goal.end_date));
        setGoalWeight(profileJson.userinfos.goal.goal_weight);
    }

    // Fetches the user's profile asynchronously and sets the profile values.
    async function fetchProfile() {
        const profileJson = await getProfile();
        setProfileValues(profileJson);
    }

    // useEffect hook to fetch the user's profile when the component is mounted.
    useEffect(() => {
        // Fetch profile on page load
        fetchProfile();
    }, []);

    // Function to format date to YYYY-MM-DD
    const formatDate = (date) => {
        if (!(date instanceof Date)) {
            throw new Error("Invalid date");
        }

        const year = date.getFullYear();
        const month = String(date.getMonth() + 1).padStart(2, "0"); // getMonth() is zero-based
        const day = String(date.getDate()).padStart(2, "0");

        return `${year}-${month}-${day}`;
    };

    // Function to save changes to the profile
    async function saveChanges() {
        try {
            // cast given date as a String into a Date type Date
            const chosenStartDate = new Date(startDate);
            const chosenEndDate = new Date(endDate);

            // Updating profile via API call
            const profileJson = await updateProfile({
                username: email,
                email: email,
                first_name: firstName,
                last_name: lastName,
                userinfos: {
                    male: male,
                    weight: weight,
                    size: size,
                    calorie_requirement: calories,
                    activity_hours_per_week: activities,
                    goal: {
                        start_date: formatDate(chosenStartDate),
                        end_date: formatDate(chosenEndDate),
                        goal_weight: goalWeight,
                    }
                }
            });

            // reloading the page in order to leave edit mode
            setEditMode(false);
            setProfileValues(profileJson);
        } catch (error) {
            console.log("Error: " + error);
        }
    }

    // Function to discard changes and reload the page
    async function discardChanges() {
        setEditMode(false);
        await fetchProfile();
    }

    // Function to validate input fields
    // Returns whether all inputs fields are valid
    const validateInput = () => {
        if (email === "" || firstName === "" || lastName === "" || size <= 0 || weight <= 0 || goalWeight <= 0 || activities < 0 || calories <= 0) {
            setIsValid(false);
            return false;
        }
        setIsValid(true);
        return true;
    };

    return (
        <div>
            <Header title="Profil"/>
            <div className={styles.back}>
                <BackButton/>
            </div>

            {/*Button to enable edit mode*/}
            <div className={styles.editButton} onClick={() => setEditMode(true)}>
                <EditButton/>
            </div>

            <div className="verticalContainer">
                <h3>Email</h3>
                <input
                    type="email"
                    id="email"
                    className="inputFieldBig"
                    value={email}
                    disabled={!editMode}
                    // updating the variable with the value, if its changed
                    onChange={(event) => setEmail(event.target.value)}
                />

                <h3>Vorname</h3>
                <input
                    type="text"
                    id="firstName"
                    className="inputFieldBig"
                    value={firstName}
                    disabled={!editMode}
                    // updating the variable with the value, if its changed
                    onChange={(event) => setFirstName(event.target.value)}
                />

                <h3>Nachname</h3>
                <input
                    type="text"
                    id="lastName"
                    className="inputFieldBig"
                    value={lastName}
                    disabled={!editMode}
                    // updating the variable with the value, if its changed
                    onChange={(event) => setLastName(event.target.value)}
                />

                <h3>Geschlecht</h3>
                <div className="gendersHolder">
                    <div className="genderHolder">
                        Männlich
                        <input
                            type="radio"
                            id="male"
                            className="inputFieldSmall"
                            value="true"
                            checked={male}
                            disabled={!editMode}
                            // updating the variable with the value, if its changed
                            onChange={() => setMale(true)}
                        />
                    </div>
                    <div className="genderHolder">
                        Weiblich
                        <input
                            type="radio"
                            id="female"
                            className="inputFieldSmall"
                            value="false"
                            checked={!male}
                            disabled={!editMode}
                            // updating the variable with the value, if its changed
                            onChange={() => setMale(false)}
                        />
                    </div>
                </div>

                <h3>Grösse</h3>
                <input
                    type="number"
                    id="size"
                    className="inputFieldSmall"
                    value={size}
                    disabled={!editMode}
                    // updating the variable with the value, if its changed
                    onChange={(event) => setSize(event.target.value)}
                />

                <h3>Gewicht</h3>
                <input
                    type="number"
                    id="weight"
                    className="inputFieldSmall"
                    value={weight}
                    disabled={!editMode}
                    // updating the variable with the value, if its changed
                    onChange={(event) => setWeight(event.target.value)}
                />

                <h3>Zielgewicht</h3>
                <input
                    type="number"
                    id="goalWeight"
                    className="inputFieldSmall"
                    value={goalWeight}
                    disabled={!editMode}
                    // updating the variable with the value, if its changed
                    onChange={(event) => setGoalWeight(event.target.value)}
                />

                <h3>Anzahl Stunden aktivität pro Woche</h3>
                <input
                    type="number"
                    id="activities"
                    className="inputFieldSmall"
                    value={activities}
                    disabled={!editMode}
                    // updating the variable with the value, if its changed
                    onChange={(event) => setActivities(event.target.value)}
                />

                <h3>Täglicher Kalorienverbrauch</h3>
                <input
                    type="number"
                    id="calories"
                    className="inputFieldSmall"
                    value={calories}
                    disabled={!editMode}
                    // updating the variable with the value, if its changed
                    onChange={(event) => setCalories(event.target.value)}
                />

                <h3>Startdatum</h3>
                <input
                    type="date"
                    id="startDate"
                    className="inputFieldBig"
                    value={formatDate(startDate)}
                    disabled={!editMode}
                    // updating the variable with the value, if its changed
                    onChange={(event) => setStartDate(new Date(event.target.value))}
                />

                <h3>Zieldatum</h3>
                <input
                    type="date"
                    id="endDate"
                    className="inputFieldBig"
                    value={formatDate(endDate)}
                    disabled={!editMode}
                    // updating the variable with the value, if its changed
                    onChange={(event) => setEndDate(new Date(event.target.value))}
                />
            </div>

            <h3 className="invalidInputText">
                {/*Displaying if the inputs are valid*/}
                {!isValid ? "Invalid input, please check your entries" : ""}
            </h3>

            <div>
                {editMode && // only display save and cancel buttons if in edit mode
                    <div>
                        <button onClick={() => {
                            // check if the values are valid, by using the written method
                            if (validateInput()) saveChanges();
                        }
                        } id="saveButton" className={styles.buttons}
                                style={{backgroundColor: "#22b90a", marginRight: "1rem"}}>
                            Änderungen speichern
                        </button>
                        <button onClick={discardChanges} id="discardButton" className={styles.buttons}>
                            Verwerfen
                        </button>
                    </div>
                }
            </div>

            <div>
                {!editMode && // only display sign out button if not in edit mode
                    <button id="signOut" onClick={logout} className={styles.buttons}>Abmelden</button>}
            </div>

            <Footer/>
        </div>
    );
}
