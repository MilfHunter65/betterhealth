// Import the necessary modules and components
import React from "react";
import ReactDOM from "react-dom/client"; // Import the createRoot API from React DOM
import "./index.css"; // Import the global CSS file for styling
import App from "./App"; // Import the main App component
import reportWebVitals from "./reportWebVitals"; // Import the reportWebVitals function

// Create a root element for the React application
const root = ReactDOM.createRoot(document.getElementById("root"));

/**
 * Render the React application.
 * Wrap the App component with React.StrictMode to highlight potential problems.
 */
root.render(
    <React.StrictMode>
        <App />
    </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
