// Import the necessary CSS file for styling the component
import "./editButton.css";
// Import the edit button icon image
import editButtonIcon from "../../images/edit-button.png";

/**
 * EditButton component renders a button with an edit icon.
 * @returns {JSX.Element} The EditButton component.
 */
export default function EditButton() {
    return (
        <button className="button">
            <img
                className="imgIcon"
                src={editButtonIcon}
                alt="editButtonIcon"
            />
        </button>
    );
}
