// Import necessary modules and components
import "./backButton.css"; // Import CSS for styling the back button
import { useNavigate } from "react-router-dom"; // Import useNavigate hook for navigation
import backButtonIcon from "../../images/back.png"; // Import the back button icon image

/**
 * BackButton component renders a button that navigates the user to the previous page.
 * @returns {JSX.Element} A button element with an image icon that, when clicked, navigates the user back one step in the history stack.
 */
export default function BackButton() {
    const navigate = useNavigate(); // Get the navigate function from the useNavigate hook

    /**
     * Handles the button click event to navigate back in the history.
     * @returns {void}
     */
    const handleClick = () => {
        navigate(-1); // Navigate back one step in the history
    };

    return (
        <>
            {/* Render button with back icon */}
            <button className="button" onClick={handleClick}>
                <img
                    className="imgIcon"
                    src={backButtonIcon}
                    alt="backButtonIcon"
                />
            </button>
        </>
    );
}
