// Import the necessary CSS file for styling the component
import "./profileButton.css";
// Import the profile button icon image
import profileButtonIcon from "../../images/account.png";
// Import the Link component from react-router-dom for navigation
import { Link } from "react-router-dom";

/**
 * ProfileButton component renders a button with a profile icon that navigates to the profile page when clicked.
 * @returns {JSX.Element} The ProfileButton component.
 */
export default function ProfileButton() {
    return (
        <Link to="/profile">
            <button className="button">
                <img
                    className="imgIcon"
                    src={profileButtonIcon}
                    alt="profileButtonIcon"
                />
            </button>
        </Link>
    );
}
