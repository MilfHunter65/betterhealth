// Import the necessary CSS file for styling the component
import styles from "./footer.module.css";

/**
 * Footer component renders a footer section with a horizontal rule and a heading.
 * @returns {JSX.Element} The Footer component.
 */
export default function Footer() {
    return (
        <>
            <div className={styles.clear}></div>
            <footer className={styles.footer}>
                <hr/>
                <h3>Betterhealth</h3>
            </footer>
        </>
    );
}
