import styles from "./calendar.module.css"; // Importing CSS module for styling
import React, { useEffect, useState } from "react"; // Importing React and hooks (useEffect, useState)
import Paper from "@material-ui/core/Paper"; // Importing Paper component from Material-UI for layout
import { Grid, Tooltip } from "@material-ui/core"; // Importing Grid and Tooltip component from Material-UI for the ui
import {
    format,
    addDays,
    getWeek,
    addWeeks,
    subWeeks,
    subDays,
} from "date-fns"; // Importing date manipulation functions from date-fns library
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos"; // Importing back arrow icon from Material-UI icons
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos"; // Importing forward arrow icon from Material-UI icons
import CalendarTodayIcon from "@material-ui/icons/CalendarToday"; // Importing calendar icon from Material-UI icons
import DatePicker from "react-datepicker"; // Importing DatePicker component from react-datepicker library
import "react-datepicker/dist/react-datepicker.css"; // Importing CSS for react-datepicker
import { getNutritionPlan } from "../../services/apiService"; // Importing getNutritionPlan function from apiService
import { Link } from "react-router-dom"; // Importing Link component from react-router-dom for navigation


/**
 * Calendar component displays a calendar view with weekly navigation and recipe information.
 * @returns {JSX.Element} The Calendar component.
 */
export default function Calendar() {
    // initializing variables using useState()
    const [currentMonth, setCurrentMonth] = useState(new Date());
    const [currentWeek, setCurrentWeek] = useState(getWeek(currentMonth));
    const [startDate, setStartDate] = useState(new Date());
    const [renderedCells, setRenderedCells] = useState(undefined);

    /**
     * Handles the week change based on the button type (previous or next).
     * @param {string} btnType - The type of button clicked ("prev" or "next").
     */
    const changeWeekHandle = (btnType) => {
        if (btnType === "prev") {
            setStartDate((date) => subDays(date, 7));
            setCurrentMonth(subWeeks(currentMonth, 1));
            setCurrentWeek(getWeek(subWeeks(currentMonth, 1)));
        }
        if (btnType === "next") {
            setStartDate((date) => addDays(date, 7));
            setCurrentMonth(addWeeks(currentMonth, 1));
            setCurrentWeek(getWeek(addWeeks(currentMonth, 1)));
        }
    };

    /**
     * Custom input component for the date picker.
     * @param {Object} props - The props for the custom input component.
     * @returns {JSX.Element} The custom input component.
     */
    const ExampleCustomInput = ({ value, onClick }) => {
        return (
            <CalendarTodayIcon className={styles.dateIcon} onClick={onClick} />
        );
    };

    /**
     * Fetches recipes for the given days and formats them for display.
     * @param {Array} daysForRecipes - The days to fetch recipes for.
     * @returns {Array} The formatted recipes components.
     */
    const getRecipes = async (daysForRecipes) => {
        // In recipes all the fetched recipes are stored. Since a week consist of 14 recipes
        // we can set predefine the Array with 14 elements
        let recipes = new Array(14);
        // In recIds are all the days stored for the request. Since there are 14 recipes we need 14 ids.
        let recIds = new Array(14);
        // Performing the request and saving the response in the arrays
        for (let day in daysForRecipes) {
            let twoRecipes;
            try {
                twoRecipes = await getNutritionPlan(daysForRecipes[day]); // request to the backend

                // Saving the recipes in the corresponding array
                recipes.splice(parseInt(day), 1, twoRecipes.recipe1.name);
                recipes.splice(parseInt(day) + 7, 1, twoRecipes.recipe2.name);
                // Saving the ids of the recipes in the corresponding array
                recIds.splice(parseInt(day), 1, twoRecipes.recipe1.id);
                recIds.splice(parseInt(day) + 7, 1, twoRecipes.recipe2.id);
            } catch (error) { // Error handling
                console.log("Error message: " + error.message);
                recipes[parseInt(day)] = "Keine Daten gefunden";
                recIds[parseInt(day)] = "";
                recipes[parseInt(day) + 7] = "Keine Daten gefunden";
                recIds[parseInt(day) + 7] = "";
            }
        }

        // Initializing the new vars needed
        let newRecipes = [];
        let newModifiedRecipes = [];

        // Looping every recipe into an array, which is shown on the overview page
        for (let x in recipes) {
            // Defining the routePath, if one recipes was clicked
            const path = "/recipe/" + recIds[x].toString();

            // If a recipe was found pushing the recipe into the newRecipes array as a button
            if (recIds[x] !== "") {
                // Linking the path, we defined before
                newRecipes.push(
                    <Link to={path} key={x}>
                        <Tooltip title={recipes[x]}>
                            <button className={styles.recipeButton}>
                                {recipes[x].substring(0, 6)} {/*only showing 6 letters of the title, since the space is limited*/}
                            </button>
                        </Tooltip>
                    </Link>
                );
            } else {
                // If no id was found we show that noting was found
                newRecipes.push(
                    <span key={x}>
                        <button
                            className={
                                styles.recipeButton + " " + styles.noRecipe
                            }>
                            {recipes[x].substring(0, 6)}
                        </button>
                    </span>
                );
            }
            // If the newRecipes array has reached 7 elements we push the array into a new Array, since a week consists of 7 days
            if (newRecipes.length === 7) {
                newModifiedRecipes.push(
                    <div key={newModifiedRecipes.length === 0 ? 0 : 1}> {/*Defining key based on the length of the newModifiedRecipes array*/}
                        {newRecipes}
                    </div>
                );
                // Clearing the array
                newRecipes = [];
            }
        }

        return newModifiedRecipes;
    };

    // Executing useEffect, when the week was changed
    useEffect(() => {
        async function renderCells() {
            // Initializing variables for the calendar and calculations
            const dateFormat = "d ";
            const dateFormatWeek = "EEE";
            const rows = [];
            let daysForRecipes = [];
            let days = [];
            let day = startDate;
            let formattedDate = "";
            let formattedDateWeek = "";
            // Adding 7 week day + number to overlay
            for (let i = 0; i < 7; i++) {
                // Formatting given date
                formattedDate = format(day, dateFormat);
                formattedDateWeek = format(day, dateFormatWeek);
                // Pushing date and number to array, which will be shown
                days.push(
                    <div
                        key={day}
                        className={styles.view}
                        style={{ display: "inline-block" }}>
                        <div className={styles.week}>{formattedDateWeek}</div>
                        <div className={styles.day}>{formattedDate}</div>
                    </div>
                );

                // Preparing array with the dates for the backend request and format the date to a string
                // Converting to format: YYYY-MM-DD
                daysForRecipes.push(
                    day.getFullYear() +
                        "-" +
                        (day.getMonth() + 1 < 10
                            ? "0" + (day.getMonth() + 1)
                            : day.getMonth() + 1) +
                        "-" +
                        (day.getDate() < 10
                            ? "0" + day.getDate()
                            : day.getDate())
                );
                // Increment 1 day
                day = addDays(day, 1);
            }

            // Clearing last element of array in order to clear it
            rows.pop();
            // Pushing the new 7 days into the array
            rows.push(<div key={day}>{days}</div>);
            // Clearing array
            days = [];

            // Executing the function getRecipes in order to get all the Recipes in the correct format
            let recipes = await getRecipes(daysForRecipes);

            // Setting the days and the recipes for the overlay to display
            setRenderedCells(
                <div className={styles.main}>
                    <Grid container>
                        <Grid item>
                            <div>{rows}</div>
                            <div>{recipes}</div>
                        </Grid>
                    </Grid>
                </div>
            );
        }
        renderCells();
    }, [currentWeek, startDate]);

    return (
        <>
            {/* Fragment to group multiple elements without adding extra nodes to the DOM */}
            <div className={styles.root}>
                {/* Material-UI Paper component with no elevation (flat look) */}
                <Paper elevation={0}>
                    <div>
                        <div className={styles.calendarWidgetOverview}>
                            {/* Wrapper for the back arrow icon */}
                            <div style={{ display: "inline-block" }}>
                                <ArrowBackIosIcon
                                    onClick={() => changeWeekHandle("prev")} // Click handler for changing to the previous week
                                    className={styles.icon} // CSS class for styling the icon
                                />
                            </div>
                            {/* Wrapper for the DatePicker component */}
                            <div style={{ display: "inline-block" }}>
                                <DatePicker
                                    selected={startDate} // Selected date state
                                    onChange={(date) => setStartDate(date)} // Change handler for updating the date
                                    customInput={<ExampleCustomInput />} // Custom input component for the DatePicker
                                />
                            </div>
                            {/* Wrapper for the forward arrow icon */}
                            <div style={{ display: "inline-block" }}>
                                <ArrowForwardIosIcon
                                    onClick={() => changeWeekHandle("next")} // Click handler for changing to the next week
                                    className={styles.icon} // CSS class for styling the icon
                                />
                            </div>
                        </div>
                    </div>
                    {/* Rendered calendar cells or content */}
                    <div>{renderedCells}</div>
                </Paper>
            </div>
        </>
    );
}
