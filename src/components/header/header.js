// Import the necessary CSS file for styling the component
import "./header.css";

/**
 * Header component renders a header section with a title and a horizontal rule.
 * @param {Object} props - The properties object.
 * @param {string} props.title - The title to be displayed in the header.
 * @returns {JSX.Element} The Header component.
 */
export default function Header(props) {
    return (
        <header>
            <div>
                <h1>{props.title}</h1>
                <hr />
            </div>
        </header>
    );
}
