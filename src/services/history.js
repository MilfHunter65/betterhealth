// Import the createBrowserHistory function from the history package
import { createBrowserHistory } from 'history';

// Create a browser history object using createBrowserHistory function
// This allows for custom navigation handling in a React application
export default createBrowserHistory();
