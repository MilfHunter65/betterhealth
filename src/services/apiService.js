import axios from "axios";
import history from "./history";

// Create an Axios instance with a base URL and default headers
const apiClient = axios.create({
    baseURL: process.env.REACT_APP_BACKEND_BASE_URL,
    headers: {
        'Content-Type': 'application/json',
    },
});

// Interceptor to include JWT token in request headers
apiClient.interceptors.request.use(
    config => {
        const token = localStorage.getItem('token');
        if (token) {
            config.headers['Authorization'] = `Bearer ${token}`
        }
        return config;
    },
    error => {
        return Promise.reject(error);
    }
);

// Interceptor to handle 401 Unauthorized responses and refresh the token or redirect to login page
apiClient.interceptors.response.use((response) => response,
    async function (error) {
        const originalRequest = error.config;
        if (error.response && error.response.status === 401 && !originalRequest._retry && !originalRequest.url.includes('api/token') && originalRequest.url !== '/api/register') {
            originalRequest._retry = true;
            try {
                const access_token = await refreshToken();
                originalRequest.headers.Authorization = `Bearer ${access_token}`;
                return apiClient(originalRequest);
            } catch (e) {
                history.push('/login');
                return Promise.reject(e);
            }
        }
        return Promise.reject(error);
    })

// Export API functions

/**
 * Registers a new user.
 * @param {Object} userData - The user data to register.
 * @returns {Object} The response data from the server.
 * @throws {Object} The error response data.
 */
export const registerUser = async (userData) => {
    try {
        const response = await apiClient.post('/api/register', userData);
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};

/**
 * Obtains a JWT token pair (access and refresh tokens) using user credentials. (Login a user)
 * @param {Object} credentials - The user credentials (username and password).
 * @returns {Object} The response data containing access and refresh tokens.
 * @throws {Object} The error response data.
 */
export const obtainToken = async (credentials) => {
    try {
        const response = await apiClient.post('/api/token/pair', credentials);
        localStorage.setItem('token', response.data.access);
        localStorage.setItem('refreshToken', response.data.refresh);
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};

/**
 * Refreshes the JWT access token using the refresh token.
 * @returns {string} The new access token.
 * @throws {Object} The error response data.
 */
export const refreshToken = async () => {
    try {
        const refreshToken = localStorage.getItem('refreshToken');
        const response = await apiClient.post('/api/token/refresh', {refresh: refreshToken});
        localStorage.setItem('token', response.data.access);
        return response.data.access;
    } catch (error) {
        throw error.response.data;
    }
};

/**
 * Verifies the validity of a JWT token.
 * @param {string} token - The token to verify.
 * @returns {Object} The response data indicating token validity.
 * @throws {Object} The error response data.
 */
export const verifyToken = async (token) => {
    try {
        const response = await apiClient.post('/api/token/verify', {token});
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};

/**
 * Logs out the user by removing tokens and redirecting to the login page.
 */
export const logout = () => {
    localStorage.removeItem("token");
    localStorage.removeItem("refreshToken");
    history.push('/login');
}

/**
 * Retrieves the user's profile data.
 * @returns {Object} The user's profile data.
 * @throws {Object} The error response data.
 */
export const getProfile = async () => {
    try {
        const response = await apiClient.get('/api/profile');
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};

/**
 * Updates the user's profile data.
 * @param {Object} profileData - The new profile data to update.
 * @returns {Object} The updated profile data.
 * @throws {Object} The error response data.
 */
export const updateProfile = async (profileData) => {
    try {
        const response = await apiClient.patch('/api/profile', profileData);
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};

/**
 * Retrieves a recipe by its ID.
 * @param {string} id - The ID of the recipe to retrieve.
 * @returns {Object} The recipe data.
 * @throws {Object} The error response data.
 */
export const getRecipe = async (id) => {
    try {
        const response = await apiClient.get(`/api/recipe/${id}`);
        return response.data;
    } catch (error) {
        throw error;
    }
};

/**
 * Retrieves an ingredient by its ID.
 * @param {string} id - The ID of the ingredient to retrieve.
 * @returns {Object} The ingredient data.
 * @throws {Object} The error response data.
 */
export const getIngredient = async (id) => {
    try {
        const response = await apiClient.get(`/api/ingredient/${id}`);
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};

/**
 * Retrieves the nutrition plan for a specific date.
 * @param {string} date - The date of the nutrition plan to retrieve.
 * @returns {Object} The nutrition plan data.
 * @throws {Object} The error response data.
 */
export const getNutritionPlan = async (date) => {
    try {
        const response = await apiClient.get(`/api/nutritionplan?date=${date}`);
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};
