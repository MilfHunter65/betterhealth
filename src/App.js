// Import necessary components and functions from react-router-dom and other local modules.
import {
    Routes,  // Component to define all the routes in the application
    Route,   // Component to define a single route
    Navigate, // Component to handle navigation and redirects
    unstable_HistoryRouter as HistoryRouter, // Router component that allows custom history object
} from "react-router-dom";
// Importing all the pages, which has to be shown
import Overview from "./pages/overview/overview"; // Import the Overview page component
import Login from "./pages/login/login"; // Import the Login page component
import Recipe from "./pages/recipe/recipe"; // Import the Recipe page component
import Profile from "./pages/profile/profile"; // Import the Profile page component
import Layout from "./pages/layout/layout"; // Import the Layout component which likely contains common UI elements like header, footer, etc.
import Register from "./pages/register/register"; // Import the Register page component

import history from "./services/history"; // Import a custom history object for navigation control

/**
 * App component sets up the routing for the application using react-router-dom.
 * @returns {JSX.Element} The App component.
 */
function App() {
    return (
        // HistoryRouter allows using a custom history object for navigation
        <HistoryRouter history={history}>
            {/* Define the routes for the application */}
            <Routes>
                {/* Layout component wraps all the routes */}
                <Route path="/" element={<Layout />}>
                    {/* Default route showing the Overview component */}
                    <Route index element={<Overview />} />
                    {/* Route for the login page */}
                    <Route path="login" element={<Login />} />
                    {/* Route for a specific recipe page, indicated by an ID parameter */}
                    <Route path="recipe/:id" element={<Recipe />} />
                    {/* Route for the user's profile page */}
                    <Route path="profile" element={<Profile />} />
                    {/* Route for the registration page */}
                    <Route path="register" element={<Register />} />
                    {/* Catch-all route that redirects to the homepage if no other route matches */}
                    <Route path="*" element={<Navigate to="/" />} />
                </Route>
            </Routes>
        </HistoryRouter>
    );
}

export default App; // Export the App component as the default export
