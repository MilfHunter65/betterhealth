from django.test import TestCase
from ninja_extra.testing import TestClient
from ninja_jwt.tokens import AccessToken

from .api import api
from .models import User, UserInfos, Goal, Recipe, NutritionValue, NutritionDayPlan, Ingredient, RecipeIngredient


class UserTest(TestCase):
    """Tests about the user and profile."""

    # User object used for the user tests.
    body = {
        "username": "test@test.com",
        "email": "test@test.com",
        "password": "sml12345.",
        "first_name": "Test",
        "last_name": "Tester",
        "male": True,
        "weight": 70,
        "size": 180,
        "calorie_requirement": 2200,
        "activity_hours_per_week": 2,
        "start_date": "2024-06-01",
        "end_date": "2024-06-02",
        "goal_weight": 65
    }

    # User object excepted in profile response.
    body_res = {
        "username": "createtest@test.com",
        "email": "createtest@test.com",
        "first_name": body["first_name"],
        "last_name": body["last_name"],
        "userinfos": {
            "male": body["male"],
            "weight": body["weight"],
            "size": body["size"],
            "calorie_requirement": body["calorie_requirement"],
            "activity_hours_per_week": body["activity_hours_per_week"],
            "goal": {
                "start_date": body["start_date"],
                "end_date": body["end_date"],
                "goal_weight": body["goal_weight"]
            }
        }
    }
    token = ""

    def setUp(self):
        """Creates the models in the database used in the user tests."""
        self.client = TestClient(api.default_router)
        goal = Goal(start_date=self.body["start_date"], end_date=self.body["end_date"],
                    goal_weight=self.body["goal_weight"])
        goal.save()
        user = User(username=self.body["username"], email=self.body["email"], password=self.body["password"],
                    first_name=self.body["first_name"], last_name=self.body["last_name"])
        user.save()
        userinfos = UserInfos(user=user, goal=goal, male=self.body["male"], weight=self.body['weight'],
                              size=self.body['size'], calorie_requirement=self.body['calorie_requirement'],
                              activity_hours_per_week=self.body['activity_hours_per_week'])
        userinfos.save()

    def testCreateUserWrongBody(self):
        """Tests if the registration of a user without a body fails."""
        response = self.client.post("/register")
        self.assertEqual(response.status_code, 422)

    def testCreateUserCorrectBody(self):
        """Tests if the registration of a user successes."""
        body = self.body
        body["username"] = self.body_res['username']
        body["email"] = self.body_res['email']
        response = self.client.post("/register", json=body)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json(), {'message': 'User created'})

    def testGetProfile(self):
        """Tests if the user can get their profile."""
        user = User.objects.first()
        token = AccessToken.for_user(user)
        response = self.client.get("/profile", headers={"Authorization": "Bearer " + str(token)})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), self.body_res)


class RecipeTest(TestCase):
    """Tests about the recipe."""

    # Recipe object used for the recipe tests.
    recipe = {
        'id': 1,
        'name': 'test recipe',
        'recipe_ingredients': [{
            'ingredient': {
                'id': 1,
                'name': 'test ingredient',
                'nutrition_values': {
                    'id': 1,
                    'energy': 1.0,
                    'fat': 2.0,
                    'carbohydrates': 3.0,
                    'dietary_fibers': 4.0,
                    'protein': 5.0,
                    'salt': 6.0,
                    'calories': 7.0
                }
            },
            'amount': 10,
            'amount_unit': 'g'
        }],
        'preparation': 'test preparation',
        'nutrition_value': {
            'id': 1,
            'energy': 1.0,
            'fat': 2.0,
            'carbohydrates': 3.0,
            'dietary_fibers': 4.0,
            'protein': 5.0,
            'salt': 6.0,
            'calories': 7.0
        }
    }

    def setUp(self):
        """Creates the models in the database used in the recipe tests."""
        self.client = TestClient(api.default_router)
        self.user = User(username="tester", password="sml12345.")
        self.user.save()
        nv = NutritionValue(energy=self.recipe['nutrition_value']['energy'], fat=self.recipe['nutrition_value']['fat'],
                            carbohydrates=self.recipe['nutrition_value']['carbohydrates'],
                            dietary_fibers=self.recipe['nutrition_value']['dietary_fibers'],
                            protein=self.recipe['nutrition_value']['protein'],
                            salt=self.recipe['nutrition_value']['salt'],
                            calories=self.recipe['nutrition_value']['calories'])
        nv.save()
        recipe = Recipe(name=self.recipe['name'], preparation=self.recipe['preparation'], nutrition_value=nv)
        recipe.save()
        ingredient = Ingredient(name=self.recipe['recipe_ingredients'][0]['ingredient']['name'], nutrition_values=nv)
        ingredient.save()
        recipe_ingredient = RecipeIngredient(ingredient=ingredient,
                                             amount=self.recipe['recipe_ingredients'][0]['amount'],
                                             amount_unit=self.recipe['recipe_ingredients'][0]['amount_unit'])
        recipe_ingredient.save()
        recipe.recipe_ingredients.add(recipe_ingredient)

    def testGetRecipe(self):
        """Tests if a user can get an existing recipe."""
        token = AccessToken.for_user(self.user)
        response = self.client.get("/recipe/1", headers={"Authorization": "Bearer " + str(token)})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), self.recipe)

    def testGetRecipeNotExist(self):
        """Tests if getting a recipe that does not exist, throws the correct error."""
        token = AccessToken.for_user(self.user)
        response = self.client.get("/recipe/5", headers={"Authorization": "Bearer " + str(token)})
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {'message': 'Recipe not found'})


class NutritionDayPlanTest(TestCase):
    """Tests about the Nutrition day plan."""
    nutrition_day_plan = {
        "recipe1": {
            "id": 1,
            "name": "test recipe"
        },
        "recipe2": {
            "id": 1,
            "name": "test recipe"
        },
        "date": "2024-06-01"
    }

    def setUp(self):
        """Creates the models in the database used in the nutrition day plans tests."""
        self.client = TestClient(api.default_router)
        self.user = User(username="tester", password="sml12345.")
        self.user.save()
        nv = NutritionValue(energy=1.0, fat=2.0, carbohydrates=3.0, dietary_fibers=4.0, protein=5.0, salt=6.0,
                            calories=7.0)
        nv.save()
        recipe = Recipe(name=self.nutrition_day_plan['recipe1']['name'], preparation="test", nutrition_value=nv)
        recipe.save()
        ndp = NutritionDayPlan(user=self.user, recipe1=recipe, recipe2=recipe, date=self.nutrition_day_plan['date'])
        ndp.save()

    def testGetNutritionDayPlan(self):
        """Tests if the user can get an existing Nutrition day plan."""
        token = AccessToken.for_user(self.user)
        response = self.client.get(f"/nutritionplan?date={self.nutrition_day_plan['date']}", headers={"Authorization": "Bearer " + str(token)})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), self.nutrition_day_plan)

    def testGetNutritionDayPlanNotExist(self):
        """Tests if getting a plan that does not exist, throws the correct error."""
        token = AccessToken.for_user(self.user)
        response = self.client.get("/nutritionplan?date=2025-01-01", headers={"Authorization": "Bearer " + str(token)})
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {'message': 'Nutrition plan not found'})
