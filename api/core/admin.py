from django.contrib import admin
from .models import Goal, UserInfos, NutritionValue, Ingredient, Recipe, NutritionDayPlan, RecipeIngredient


class NutritionDayPlanAdmin(admin.ModelAdmin):
    """Admin page display fields for NutritionDayPlan model"""
    list_display = ['user', 'recipe1', 'recipe2', 'date']


class RecipeIngredientAdmin(admin.ModelAdmin):
    """Admin page display fields for RecipeIngredient model"""
    list_display = ['ingredient', 'amount', 'amount_unit']


# Register all Models for the Admin page.
admin.site.register(Goal)
admin.site.register(UserInfos)
admin.site.register(NutritionValue)
admin.site.register(Ingredient)
admin.site.register(RecipeIngredient, RecipeIngredientAdmin)
admin.site.register(Recipe)
admin.site.register(NutritionDayPlan, NutritionDayPlanAdmin)
