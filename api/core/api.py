from django.core.exceptions import ValidationError
from django.db.utils import IntegrityError

from django.contrib.auth.models import User
from django.contrib.auth import password_validation
from ninja_extra import NinjaExtraAPI
from ninja_jwt.controller import NinjaJWTDefaultController
from ninja_jwt.authentication import JWTAuth

from .models import UserInfos, Goal, Recipe, Ingredient, NutritionDayPlan
from .schemas import Response, Register, Profile, RecipeSchema, IngredientSchema, NutritionDayPlanSchema, PasswordValidationErrorSchema
from datetime import date, timedelta
from .helper import ai, parser

# setup ninja
api = NinjaExtraAPI(auth=[JWTAuth()])

# register controller for the JWT plugin
api.register_controllers(NinjaJWTDefaultController)


@api.post("/register", response={201: Response, 409: Response, 400: PasswordValidationErrorSchema}, auth=None)
def register(request, payload: Register):
    """Creates a user with the user infos from the body and generates a nutrition plan.
    It Returns a confirmation that the user was created.
    It will fail if the username already exists or the password is weak."""

    try:
        password_validation.validate_password(payload.password)
    except ValidationError as e:
        return 400, {"errors": e.messages}
    try:
        user = User.objects.create_user(
            payload.username,
            payload.email,
            payload.password,
            first_name=payload.first_name,
            last_name=payload.last_name,
        )
    except IntegrityError:
        return 409, {"message": "User already exists"}

    goal = Goal(start_date=payload.start_date, end_date=payload.end_date, goal_weight=payload.goal_weight)
    goal.save()

    userinfos = UserInfos(
        user=user,
        goal=goal,
        male=payload.male,
        weight=payload.weight,
        size=payload.size,
        calorie_requirement=payload.calorie_requirement,
        activity_hours_per_week=payload.activity_hours_per_week
    )
    userinfos.save()

    dayscount = userinfos.goal.end_date - userinfos.goal.start_date
    gender = "Männlich" if userinfos.male else "Weiblich"

    # generate the nutrition plan
    ai_nutritionplan = ai.generate_nutrition_plan(dayscount.days,
                                                  gender,
                                                  userinfos.weight,
                                                  userinfos.size,
                                                  userinfos.calorie_requirement,
                                                  userinfos.activity_hours_per_week,
                                                  userinfos.goal.goal_weight)

    nutritionplandate = userinfos.goal.start_date

    # generate the recipes if it does not already exists
    for recipe in ai_nutritionplan:
        existingrecipe1 = Recipe.objects.filter(name__contains=recipe['recipe1'])
        if not existingrecipe1.exists():
            recipe1 = ai.generate_recipe(recipe['recipe1'])
            newrecipe1 = parser.create_recipe(recipe1, recipe['recipe1'])
        else:
            newrecipe1 = existingrecipe1.first()

        existingrecipe2 = Recipe.objects.filter(name__contains=recipe['recipe2'])
        if not existingrecipe2.exists():
            recipe2 = ai.generate_recipe(recipe['recipe2'])
            newrecipe2 = parser.create_recipe(recipe2, recipe['recipe2'])
        else:
            newrecipe2 = existingrecipe2.first()

        newnutritiondayplan = NutritionDayPlan(user=user, recipe1=newrecipe1, recipe2=newrecipe2,
                                               date=nutritionplandate)
        newnutritiondayplan.save()
        nutritionplandate = nutritionplandate + timedelta(days=1)

    return 201, {"message": "User created"}


@api.get("/profile", response={200: Profile})
def profile(request):
    """Returns the information of the user."""

    user = request.user
    return 200, user


@api.patch("/profile", response={200: Profile, 409: Response})
def update_profile(request, payload: Profile):
    """Patches some fields of the logged-in user."""

    user = request.user

    # only update the given attributes
    for attr, value in payload.dict(exclude_unset=True).items():
        if attr == "userinfos":
            userinfos = user.userinfos
            for userinfosattr, userinfosvalue in value.items():
                if userinfosattr == "goal":
                    goal = userinfos.goal
                    for goalattr, goalvalue in userinfosvalue.items():
                        setattr(goal, goalattr, goalvalue)
                    goal.save()
                else:
                    setattr(userinfos, userinfosattr, userinfosvalue)
            userinfos.save()
        else:
            setattr(user, attr, value)
    user.save()
    return 200, user


@api.get("/recipe/{recipe_id}", response={200: RecipeSchema, 404: Response})
def get_recipe(request, recipe_id: int):
    """Returns the recipe with the ingredients."""

    try:
        recipe = Recipe.objects.get(id=recipe_id)
    except Recipe.DoesNotExist:
        return 404, {"message": "Recipe not found"}
    return 200, recipe


@api.get("/ingredient/{ingredient_id}", response={200: IngredientSchema, 404: Response})
def get_ingredient(request, ingredient_id: int):
    """Returns the ingredient. Not used at the moment."""

    try:
        ingredient = Ingredient.objects.get(id=ingredient_id)
    except Ingredient.DoesNotExist:
        return 404, {"message": "Ingredient not found"}
    return 200, ingredient


@api.get("/nutritionplan", response={200: NutritionDayPlanSchema, 404: Response})
def get_nutrition_plan_per_day(request, date: date):
    """Returns the nutrition plan of the logged-in user for the date in the date url parameter."""

    nutrition_plan = NutritionDayPlan.objects.filter(user=request.user, date=date).first()
    if nutrition_plan:
        return 200, nutrition_plan
    else:
        return 404, {"message": "Nutrition plan not found"}
