from typing import List

from ninja import Schema, ModelSchema
from datetime import date

from .models import NutritionValue


class Response(Schema):
    """General Response schema."""
    message: str


class Register(Schema):
    """schema for register user."""
    username: str
    email: str
    password: str
    first_name: str
    last_name: str
    male: bool
    weight: int
    size: int
    calorie_requirement: int
    activity_hours_per_week: int
    start_date: date
    end_date: date
    goal_weight: int


class GoalSchema(Schema):
    """Goal schema used in UserInfosschema."""
    start_date: date = None
    end_date: date = None
    goal_weight: int = None


class UserInfosSchema(Schema):
    """User info schema used in Profile."""
    male: bool = None
    weight: int = None
    size: int = None
    calorie_requirement: int = None
    activity_hours_per_week: int = None
    goal: GoalSchema = None


class Profile(Schema):
    """Profile schema used in GET and PATCH profile."""
    username: str = None
    email: str = None
    first_name: str = None
    last_name: str = None
    userinfos: UserInfosSchema = None


class NutritionValuesSchema(ModelSchema):
    """Nutrition value schema used in recipe and ingredient schema.
    This schema is automatically generated with the Model."""
    class Meta:
        model = NutritionValue
        fields = '__all__'


class IngredientSchema(Schema):
    """Ingredient schema used in GET ingredient and recipe ingredient schema."""
    id: int
    name: str
    nutrition_values: NutritionValuesSchema


class RecipeIngredientSchema(Schema):
    """Ingredient schema used recipe schema."""
    ingredient: IngredientSchema
    amount: int
    amount_unit: str


class RecipeSchema(Schema):
    """Recipe schema used GET recipe."""
    id: int
    name: str
    recipe_ingredients: List[RecipeIngredientSchema]
    preparation: str
    nutrition_value: NutritionValuesSchema


class RecipeSimpleSchema(Schema):
    """Recipe schema with only id and name used in nutrition day plan schema."""
    id: int
    name: str


class NutritionDayPlanSchema(Schema):
    """Nutrition day plan schema used in GET nutrition day plan."""
    recipe1: RecipeSimpleSchema
    recipe2: RecipeSimpleSchema
    date: date


class PasswordValidationErrorSchema(Schema):
    """Schema used for password validation error, when the password does not meet the requirements."""
    errors: List[str]
