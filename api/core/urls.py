from django.urls import path

from .api import api

# register the url endpoints for the api
urlpatterns = [
    path('', api.urls)
]
