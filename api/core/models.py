from django.db import models
from django.contrib.auth.models import User


class Goal(models.Model):
    """Goals of a user. Used in UserInfos model."""

    start_date = models.DateField()
    end_date = models.DateField()
    goal_weight = models.IntegerField()

    def __str__(self):
        return f'{self.start_date} {self.end_date} - {self.goal_weight}'


class UserInfos(models.Model):
    """Model used to extend the fields of the User model from django auth by a one-to-one relation."""

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    goal = models.OneToOneField(Goal, on_delete=models.CASCADE)
    male = models.BooleanField(default=True)
    weight = models.IntegerField()
    size = models.IntegerField()
    calorie_requirement = models.IntegerField()
    activity_hours_per_week = models.IntegerField()

    def __str__(self):
        return self.user.username


class NutritionValue(models.Model):
    """Nutrition values of a recipe or ingredient."""

    energy = models.FloatField()
    fat = models.FloatField()
    carbohydrates = models.FloatField()
    dietary_fibers = models.FloatField()
    protein = models.FloatField()
    salt = models.FloatField()
    calories = models.FloatField()


class Ingredient(models.Model):
    """Ingredient model with a one-to-one relation with the nutrition values."""

    name = models.CharField(max_length=75, unique=True)
    nutrition_values = models.OneToOneField(NutritionValue, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class RecipeIngredient(models.Model):
    """Intermediate table bettween a recipe and ingredient with the amount of that ingredient and the amount unit."""

    ingredient = models.ForeignKey(Ingredient, on_delete=models.CASCADE)
    amount = models.IntegerField()
    amount_unit = models.CharField(max_length=25)

    def __str__(self):
        return f'{self.ingredient} {self.amount} {self.amount_unit}'


class Recipe(models.Model):
    """Recipe model with a one-to-one relation with the nutrition values."""

    name = models.CharField(max_length=100, unique=True)
    recipe_ingredients = models.ManyToManyField(RecipeIngredient)
    preparation = models.CharField(max_length=5000)
    nutrition_value = models.OneToOneField(NutritionValue, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class NutritionDayPlan(models.Model):
    """Nutrition plan model for a day with a relation to the two recipes."""

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    recipe1 = models.ForeignKey(Recipe, on_delete=models.CASCADE, related_name='recipe1')
    recipe2 = models.ForeignKey(Recipe, on_delete=models.CASCADE, related_name='recipe2')
    date = models.DateField()

    def __str__(self):
        return f'{self.user.username} {self.date}'
