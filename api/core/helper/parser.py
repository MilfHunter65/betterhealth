from ..models import Ingredient, NutritionValue, RecipeIngredient, Recipe


def create_recipe(recipe_json: dict, name: str) -> Recipe:
    """Creates a recipe model, a nutrition value model and a ingredient model for each ingredent
    for a recipe from a json of a recipe generated with gpt.

    Parameters:
    recipe_json (dict): Recipe details (ingredients, preparation, ...) generated with gpt
    name (str): Recipe name

    Returns:
    Recipe: New Recipe created

    """

    newingredients = []

    # iterate over each ingredient from gpt
    for ingredients in recipe_json['ingredients']:
        existingingredient = Ingredient.objects.filter(name__iexact=ingredients['name'])

        # does not create the ingredient if it already exists, it just references it to the recipe model
        if not existingingredient.exists():
            nutritions = NutritionValue(energy=ingredients['energy'],
                                        fat=ingredients['fatness'],
                                        carbohydrates=ingredients['carbohydrates'],
                                        dietary_fibers=ingredients['dietary_fibers'],
                                        protein=ingredients['protein'],
                                        salt=ingredients['salt'],
                                        calories=ingredients['calories'])
            nutritions.save()
            newingredient = Ingredient(name=ingredients['name'], nutrition_values=nutritions)
            newingredient.save()
            newrecipeingredient = RecipeIngredient(ingredient=newingredient, amount=ingredients['amount'],
                                                   amount_unit=ingredients['amount_unit'])
            newrecipeingredient.save()
            newingredients.append(newrecipeingredient)
        else:
            newrecipeingredient = RecipeIngredient(ingredient=existingingredient.first(), amount=ingredients['amount'],
                                                   amount_unit=ingredients['amount_unit'])
            newrecipeingredient.save()
            newingredients.append(newrecipeingredient)

    # calculate the recipie nutrition values
    energy = fat = carbohydrates = dietary_fibers = protein = salt = calories = 0.0
    for ingre in newingredients:
        energy += ingre.ingredient.nutrition_values.energy
        fat += ingre.ingredient.nutrition_values.fat
        carbohydrates += ingre.ingredient.nutrition_values.carbohydrates
        dietary_fibers += ingre.ingredient.nutrition_values.dietary_fibers
        protein += ingre.ingredient.nutrition_values.protein
        salt += ingre.ingredient.nutrition_values.salt
        calories += ingre.ingredient.nutrition_values.calories

    recipe_nutrition = NutritionValue(energy=energy, fat=fat, carbohydrates=carbohydrates,
                                      dietary_fibers=dietary_fibers, protein=protein, salt=salt, calories=calories)
    recipe_nutrition.save()
    newrecipe = Recipe(name=name, preparation=recipe_json['preparation'], nutrition_value=recipe_nutrition)
    newrecipe.save()

    # finally add the ingredients to the recipe model
    for ingre in newingredients:
        newrecipe.recipe_ingredients.add(ingre)

    return newrecipe
