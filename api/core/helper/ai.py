from openai import OpenAI
from django.conf import settings
import json
from jsonschema import validate, ValidationError

# Setup the OpenAI client with the api key from the settings resp. the environment variable
client = OpenAI(api_key=settings.OPENAI_AI_KEY)

# The expected json schemas from the responses from gpt3.5
plan_schema = {
    "type": "object",
    "properties": {
        "recipe1": {"type": "string"},
        "recipe2": {"type": "string"},
    },
    "additionalProperties": False,
    "minProperties": 2
}

recipe_schema = {
    "type": "object",
    "properties": {
        "ingredients": {"type": "array"},
        "preparation": {"type": "string"},
    },
    "additionalProperties": False,
    "minProperties": 2
}

ingredients_schema = {
    "type": "object",
    "properties": {
        "name": {"type": "string"},
        "amount": {"type": "number"},
        "amount_unit": {"type": "string"},
        "energy": {"type": "number"},
        "fatness": {"type": "number"},
        "carbohydrates": {"type": "number"},
        "dietary_fibers": {"type": "number"},
        "protein": {"type": "number"},
        "salt": {"type": "number"},
        "calories": {"type": "number"},
    },
    "additionalProperties": False,
    "minProperties": 10
}


def gpt(messages: list) -> dict:
    """Generates a chat completion with gpt3.5 based on a list of messages.

    Parameters:
    messages (list): A list of system and user messages

    Returns:
    dict: Chat completion response

    """

    chat_completion = client.chat.completions.create(
        messages=messages,
        model="gpt-3.5-turbo",
    )
    return chat_completion.to_dict()


def generate_nutrition_plan(days: int, gender: str, weight: int, size: int, calorie_requirement: int,
                            activity_hours_per_week: int, goal_weight: int) -> list:
    """Generates a nutrition plan with gpt based on the user infos about and the number of days.

    Parameters:
    days (int): Number of days to generate (2 Recipes per day)
    gender (str): Gender of the user (male or female)
    weight (int): Current weight of the user
    size (int): Current size of the user
    calorie_requirement (int): Calorie requirements per day for the user
    activity_hours_per_week (int): activity hours per week of the user
    goal_weight (int): Goal weight of the user after the given days

    Returns:
    list: Nutrition plan for each day

    """

    messages = [
        {
            "role": "system",
            "content": 'Your are an expert for creating nutrition plans. Only answer in German except for the '
                       'attributes of the JSON (these should be in english) AND ONLY respond with a JSON list in '
                       'this format: [{"recipe1": "<Your Ricipe Name>", "recipe2": "<Your_ricipe>"}, '
                       '{ next day... You Replace "<Your ricipe>" with a correct value. Per Day you need 2 Recipes '
                       '(like in the example). Do NOT answer in markdown format, only respond with a JSON list. If '
                       'there are many days, you can also reuse recipes.',
        },
        {
            "role": "user",
            "content": f"Erstelle einen Ernährungplan für {str(days)} Tage für folgende Person: Geschlecht: {gender}, "
                       f"Gewicht: {str(weight)}, Grösse: {size}, Kalorienbedarf pro Tag: {calorie_requirement}, "
                       f"Sportliche Aktivitätsstunden per Woche: {activity_hours_per_week}, Ziel Gewicht nach "
                       f"{str(days)} Tagen: {str(goal_weight)}"
        }
    ]

    nutrition_plan_list = []

    # try up to 5 times to get a correct response from gpt
    for i in range(0, 5):
        res = str(gpt(messages)['choices'][0]['message']['content']).replace("\n", "")
        print(res)
        try:
            res_json = json.loads(res)
            for item in res_json:
                # validate each day
                validate(item, plan_schema)
            print("is valid")
            nutrition_plan_list.extend(res_json)
            # check if the number of days correspond, else ask more to gpt
            if len(nutrition_plan_list) >= days:
                # check if it has not too many days in the plan, else remove the last ones
                if len(nutrition_plan_list) > days:
                    nutrition_plan_list = nutrition_plan_list[:days]
                # return the final list
                return nutrition_plan_list
            else:
                print("no enough days, asking more to gpt")
        except (ValueError, ValidationError) as e:
            # the response from gpt does not match the expected pattern
            print("checks failed, error:" + str(e))
    return []


def generate_recipe(recipe_name: str) -> dict:
    """Generates a recipe with gpt based on a recipe name.

    Parameters:
    recipe_name (str): Name of the recipe

    Returns:
    dict: Recipe details (ingredients, preparation, ...)
    """

    messages = [
        {
            "role": "system",
            "content": 'Your are an expert for cooking. You create recipes based on the title of the recipe. '
                       'Only answer in German except for the attributes of the JSON (these should be in english) '
                       'AND ONLY respond with a JSON with this format: {"ingredients": [{"name": "<recipe_name>", '
                       '"amount": <amount>, "amount_unit": "<amount_unit>", "energy": <energy>, "fatness": <fatness>, '
                       '"carbohydrates": <carbohydrates>, "dietary_fibers": <dietry fibers>, "protein": <protein>, '
                       '"salt": <salt>, "calories": <calories>}], "preparation": "<preparation>"} Replace everywhere '
                       'where there are <> with your correct value. Put all ingredients in the array. '
                       'Do NOT answer in markdown format, only respond with a JSON.',
        },
        {
            "role": "user",
            "content": f"Recipe name: {recipe_name}"
        }
    ]

    # try up to 5 times to get a correct response from gpt
    for i in range(0, 5):
        res = str(gpt(messages)['choices'][0]['message']['content']).replace("\n", "")
        print(res)
        try:
            res_json = json.loads(res)
            # check if the response from gpt follows the correct pattern
            validate(res_json, recipe_schema)
            for ingredient in res_json['ingredients']:
                # check each recipe if the response from gpt follows the correct pattern
                validate(ingredient, ingredients_schema)
            print("is valid")
            return res_json
        except (ValueError, ValidationError) as e:
            # the response from gpt does not match the expected pattern
            print("checks failed, error:" + str(e))
    return {}
