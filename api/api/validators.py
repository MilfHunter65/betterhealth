from django.core.exceptions import ValidationError
import string


class MinimumNumberValidator:
    """Validates that a password has at least one number."""
    def __init__(self, minimum=1):
        self.minimum = minimum

    def validate(self, password, user=None):
        if not any(char.isdigit() for char in password):
            raise ValidationError("Your password must contain at least one number")

    def get_help_text(self):
        return "Your password must contain at least one number"


class ContainsSpecialCharacterValidator:
    """Validates that a password has at least one special character."""
    def __init__(self, minimum=1):
        self.minimum = minimum

    def validate(self, password, user=None):
        special_characters = string.punctuation
        if not any(char in special_characters for char in password):
            raise ValidationError("Your password must contain at least one special character")

    def get_help_text(self):
        return "Your password must contain at least one special character"
